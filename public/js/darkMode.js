// Dark-mode + local storage

let darkMode = localStorage.getItem("darkMode");

const darkModeToggle = document.querySelector("#mode");
const dropdown_menu = document.querySelectorAll(".dropdown-menu");
const dropdown_item = document.querySelectorAll(".toolbar-item");
const cards = document.querySelectorAll(".kortti");
const lisaa_btn = document.querySelectorAll(".tilaaYlos");
const korttiOts = document.querySelectorAll(".korttiOts");
const korttiHinta = document.querySelectorAll(".korttiHinta");
const koriTable = document.querySelectorAll(".koriTable");
const viivaaOstoskori = document.querySelectorAll(".viivaaOstoskori");
const viivaaOstoskoriP = document.querySelectorAll(".viivaaOstoskoriPaksu");
const footermainIcon = document.querySelectorAll(".footerMainIcon");
const footerLines = document.querySelectorAll(".light");



// Onko dark-mode päällä?
// Jos on päällä, laitetaan pois päältä
// Jos ei ole päällä, laitetaan päälle

const enableDarkMode = function() {
    // 1. Lisää class darkMode sivulle
    document.getElementById("ball").style.transform = "translateX(24px)";
    document.getElementById("ball").style.backgroundColor = "#7dd2d9";

    document.body.classList.add("dark");

    document.getElementById("toolbar").classList.add("dark");
    document.getElementById("toolbar-collapse").classList.add("dark");
    // document.getElementById("dropdown-menu").classList.add("dark");
    document.getElementById("navTuoteryhma").classList.add("dark");

    document.getElementById("footer").classList.add("dark");

    for(i = 0; i < dropdown_menu.length; i++) {
        dropdown_menu[i].style.backgroundColor = "#121212";
    }


    for(i = 0; i < dropdown_item.length; i++) {
        dropdown_item[i].style.color = "#ffffff";
    }

    for(i = 0; i < cards.length; i++) {
        cards[i].style.backgroundColor = "#121212";
        cards[i].style.border = "solid thin #7dd2d9";
        cards[i].style.color = "#ffffff";
    }

    for(i = 0; i < korttiOts.length; i++) {
        korttiOts[i].style.color = "#ffffff";
    }

    for(i = 0; i < korttiHinta.length; i++) {
        korttiHinta[i].style.color = "#ffffff";
    }

    for(i = 0; i < lisaa_btn.length; i++) {
        lisaa_btn[i].style.backgroundColor = "#7dd2d9";
        lisaa_btn[i].style.color = "#ffffff";
    }

    for(i = 0; i < koriTable.length; i++) {
        koriTable[i].style.color = "#ffffff";
    } 

    for(i = 0; i < viivaaOstoskori.length; i++) {
        viivaaOstoskori[i].style.borderBottom = "1px solid #7dd2d9";
    }

    for(i = 0; i < viivaaOstoskoriP.length; i++) {
        viivaaOstoskoriP[i].style.borderBottom = "3px solid #7dd2d9";
    }

    for(i = 0; i < footermainIcon.length; i++) {
        footermainIcon[i].style.color = "#ff33cc";
    }

    for(i = 0; i < footerLines.length; i++) {
        footerLines[i].style.borderTop = "1px solid #7dd2d9";
    }

    // 2. Päivitä darkMode locaslStorage
    localStorage.setItem("darkMode", "enabled");
};


// ---------------------------------------------------------------------------------------------


const disableDarkMode = function() {
    // 1. Lisää class darkMode sivulle
    document.getElementById("ball").style.transform = "translateX(0px)";
    document.getElementById("ball").style.backgroundColor = "#ffffff";

    document.body.classList.remove("dark");

    document.getElementById("toolbar").classList.remove("dark");
    document.getElementById("toolbar-collapse").classList.remove("dark");
    // document.getElementById("dropdown-menu").classList.remove("dark");
    document.getElementById("navTuoteryhma").classList.remove("dark");

    document.getElementById("footer").classList.remove("dark");

    for(i = 0; i < dropdown_menu.length; i++) {
        dropdown_menu[i].style.backgroundColor = "#ffffff";
    }


    for(i = 0; i < dropdown_item.length; i++) {
        dropdown_item[i].style.color = "black";
    }

    for(i = 0; i < cards.length; i++) {
        cards[i].style.backgroundColor = "#ffffff";
        cards[i].style.border = "solid thin lightgrey";
    }

    for(i = 0; i < korttiOts.length; i++) {
        korttiOts[i].style.color = "#29293d";
    }

    for(i = 0; i < korttiHinta.length; i++) {
        korttiHinta[i].style.color = "#29293d";
    }

    for(i = 0; i < lisaa_btn.length; i++) {
        lisaa_btn[i].style.backgroundColor = "#3f3f3f";
        lisaa_btn[i].style.color = "#ffffff";
    }

    for(i = 0; i < koriTable.length; i++) {
        koriTable[i].style.color = "#212529";
    } 

    for(i = 0; i < viivaaOstoskori.length; i++) {
        viivaaOstoskori[i].style.borderBottom = "1px solid #cccccc";
    }

    for(i = 0; i < viivaaOstoskoriP.length; i++) {
        viivaaOstoskoriP[i].style.borderBottom = "3px solid #cccccc";
    }

    for(i = 0; i < footermainIcon.length; i++) {
        footermainIcon[i].style.color = "#ffffff";
    }

    for(i = 0; i < footerLines.length; i++) {
        footerLines[i].style.borderTop = "1px solid #d5d5d5";
    }


    // 2. Päivitä darkMode locaslStorage
    localStorage.setItem("darkMode", null);
};

if (darkMode === "enabled") {
    enableDarkMode();
}

darkModeToggle.addEventListener("change", function() {
    
    darkMode = localStorage.getItem("darkMode");

    if (darkMode !== "enabled") {
        enableDarkMode();
    }
    else {
        disableDarkMode();
    }
});
