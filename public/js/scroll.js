const scrollUp = document.getElementById("scrollUp");
const scrollDown = document.getElementById("scrollDown");

scrollUp.addEventListener("click", function() {
    window.scrollTo({
        top: 0,
        left: 0,
    });
});

scrollDown.addEventListener("click", function() {
    window.scrollTo({
        top: document.body.scrollHeight,
        left: 0
    });
});