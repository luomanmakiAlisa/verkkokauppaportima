<?php

namespace App\Controllers;


use App\Models\TuoteryhmaModel;
use App\Models\OstoskoriModel;
use App\Models\LoginModel;

class Login extends BaseController
{
    private $tuoteryhmaModel = null;
    private $ostoskoriModel = null;

    function __construct()
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->ostoskoriModel = new OstoskoriModel();

        $session = \Config\Services::session();
        $session->start();
    }

    public function index()
    {
        echo view('templates/header');
        echo view('Login/login');
    }

    public function register()
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
        if (isset($_SESSION['kayttaja'])) {
            $data['kayttaja'] = $_SESSION['kayttaja']->etunimi;
        }
        echo view('templates/header');
        echo view('templates/nav', $data);
        echo view('login/register');
        echo view('templates/footer');
    }

    public function registeration()
    {
        $model = new LoginModel();

        if (!$this->validate([
            'kayttajanimi' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'vahvistasalasana' => 'required|min_length[8]|max_length[30]|matches[salasana]',
            'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[asiakas.email]',
            'postinro' => 'required|min_length[5]|max_length[5]|numeric'
        ])) {
            $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
            $data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
            if (isset($_SESSION['kayttaja'])) {
                $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
            }
            echo view('templates/header');
            echo view('templates/nav', $data);
            echo view('login/register');
            echo view('templates/footer');
        } else {
            $model->save([
                'kayttajanimi' => $this->request->getVar('kayttajanimi'),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'email' => $this->request->getVar('email'),
                'osoite' => $this->request->getVar('osoite'),
                'postinro' => $this->request->getVar('postinro'),
                'kaupunki' => $this->request->getVar('kaupunki'),
                'salasana' => password_hash($this->request->getVar('salasana'), PASSWORD_DEFAULT)

            ]);
            return redirect('login');
        }
    }

    public function check()
    {

        $model = new LoginModel();

        if (!$this->validate([
            'kayttajanimi' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]'
        ])) {
            echo view('templates/header');
            echo view('login/login');
        } else {
            $user = $model->check(
                $this->request->getVar('kayttajanimi'),
                $this->request->getVar('salasana')
            );
            if ($user) {
                $_SESSION['kayttaja'] = $user;
                return redirect()->to(site_url('home/index'));
            } else {
                return redirect('login');
            }
        }
    }

    public function kirjaudu()
    {

        echo view('templates/header');

        echo view('Login/login');
    }

    public function kirjaudu_ulos()
    {
        session()->destroy();
        return redirect()->to("/");
    }
}
