<?php

namespace App\Controllers;

use App\Models\AdminLoginModel;

class AdminLogin extends BaseController
{

    function __construct()
    {
        $session = \Config\Services::session();
        $session->start();
    }

    public function index()
    {
        echo view("templates/header");
        echo view("login/adminLogin");
        // echo view("templates/footer");

    }

    public function admin_register()
    {

        if(!isset($_SESSION["admintunnus"]))
		{
			return redirect("adminlogin");
		}

        echo view('templates/header');
        echo view('login/adminRegister');
        echo view('templates/footer');
    }

    public function admin_registration()
    {
        $model = new AdminLoginModel();

        if (!$this->validate([
            'admintunnus' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'vahvistasalasana' => 'required|min_length[8]|max_length[30]|matches[salasana]'
        ])) {

            echo view('templates/header');
            echo view('login/adminregister');
            echo view('templates/footer');

        } else {
            $model->save([
                'admintunnus' => $this->request->getVar('admintunnus'),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'salasana' => password_hash($this->request->getVar('salasana'), PASSWORD_DEFAULT)
            ]);
            // return redirect('adminlogin');
            return redirect()->to(site_url("admin"));
        }
    }

    public function admin_check()
    {
        $model = new AdminLoginModel();

        if (!$this->validate([
            'admintunnus' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]'
        ])) {
            echo view('templates/header');
            echo view('login/adminlogin');
        } else {
            $user = $model->admin_check(
                $this->request->getVar('admintunnus'),
                $this->request->getVar('salasana')
            );
            if ($user) {
                $_SESSION['admintunnus'] = $user;
                return redirect()->to(site_url("admin"));
            } else {
                return redirect()->to(site_url("adminlogin"));
            }
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to("/adminlogin");
    }
}