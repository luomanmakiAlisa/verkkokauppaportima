<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\OstoskoriModel;

class Home extends BaseController
{
	private $tuoteryhmaModel = null;
	private $tuoteModel = null;
	private $ostoskoriModel = null;
	function __construct()
	{
		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->tuoteModel = new TuoteModel();
		$this->ostoskoriModel = new OstoskoriModel();
		$session = \Config\Services::session();
		$session->start();
	}

	public function index()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
		if (isset($_SESSION['kayttaja'])) {
			$data['etunimi'] = $_SESSION['kayttaja']->etunimi;
		}
		echo view('templates/header');
		echo view('templates/nav', $data);
		echo view('etusivu');
		echo view('templates/footer');
	}

	public function yhteystieto()
	{
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
		if (isset($_SESSION['kayttaja'])) {
			$data['etunimi'] = $_SESSION['kayttaja']->etunimi;
		}
		echo view('templates/header');
		echo view('templates/nav', $data);
		echo view('yhteystieto');
		echo view('templates/footer');
	}
}
