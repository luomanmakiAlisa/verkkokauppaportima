<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\OstoskoriModel;
use App\Models\AsiakasModel;
use App\Models\LoginModel;

class Ostoskori extends BaseController
{
    private $tuoteryhmaModel = null;
    private $OstoskoriModel = null;
    private $asiakasModel = null;
    private $loginsModel = null;

    public function __construct()
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->ostoskoriModel = new OstoskoriModel();
        $this->asiakasModel = new AsiakasModel();
        $this->loginModel = new LoginModel();

        $session = \Config\Services::session();
        $session->start();
    }

    public function index() // ostoskori näkymä
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        // Istuntomuuttuja sisältää suoraan taulukon tuotteita eli ei ole tarvetta hakea mitään 
        // tietoja tietokannasta.
        $data['tuotteet'] = $this->ostoskoriModel->ostoskori();
        $data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
        if (isset($_SESSION['kayttaja'])) {
            $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
        }
        echo view('templates/header');
        echo view('templates/nav', $data);
        echo view('ostoskori/sisalto', $data);
        echo view('templates/footer');
    }
    /**
     * Lisää tuotteen tuotesivun kautta ja tuoteryhmäsivun kautta
     */
    public function lisaa($tuote_id)
    {
        $this->ostoskoriModel->lisaa($tuote_id);
        //return redirect()->to(site_url('/kauppa/tuote/' . $tuote_id));
        return redirect()->back();
    }

    /**
     * Poistaa tuotteen ostoskorin kautta
     */
    public function ostoskori_poista($tuote_id)
    {
        $this->ostoskoriModel->poista($tuote_id);
        return redirect()->to(site_url('ostoskori/index'));
    }
    /**
     * Lisää tuotteen ostoskorin kautta
     */
    public function ostoskori_lisaa($tuote_id)
    {
        $this->ostoskoriModel->lisaa($tuote_id);
        return redirect()->to(site_url('ostoskori/index'));
    }
    /**
     * Tyhjentää ostoskorin.
     */
    public function tyhjenna()
    {
        $this->ostoskoriModel->tyhjenna();
        return redirect()->to(site_url('ostoskori/index'));
    }


    /**
     * Poistaa yhden tuotteen ostoskorista.
     */
    public function poistaTuote($tuote_id)
    {
        $this->ostoskoriModel->poistaTuote($tuote_id);
        return redirect()->back();
    }

    public function asiakastiedot()
    {
        if (isset($_SESSION['kayttaja'])) {
            $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
            $data['osoite'] = $_SESSION['kayttaja']->osoite;
            $data['postinro'] = $_SESSION['kayttaja']->postinro;
            $data['kaupunki'] = $_SESSION['kayttaja']->kaupunki;

            echo view('templates/header');
            echo view('templates/nav_tilaus');
            echo view('tilaa/asiakasSisalla', $data);
            echo view('templates/footer');
        } else {
            echo view('templates/header');
            echo view('templates/nav_tilaus');
            echo view('tilaa/asiakas');
            echo view('templates/footer');
        }
    }

    /**
     * Tallentaa tilauksen.
     */
    public function tilaa()
    {
        if ($this->request->getMethod() === 'post') {
            if (!$this->validate([
                'osoite' => 'required|min_length[1]|max_length[30]',
                'postinro' => 'required|min_length[5]|max_length[5]|numeric',
                'kaupunki' => 'required|min_length[3]|max_length[30]'
            ])) {
                $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
                $data['osoite'] = $this->request->getPost('osoite');
                $data['postinro'] = $this->request->getPost('postinro');
                $data['kaupunki'] = $this->request->getPost('kaupunki');
                echo view('templates/header');
                echo view('templates/nav_tilaus');
                echo view('tilaa/asiakasSisalla', $data);
                echo view('templates/footer');
            } else { //Tallennetaan
                $asiakas = [
                    'id' => $_SESSION['kayttaja']->id,
                    'kayttajanimi' => $_SESSION['kayttaja']->kayttajanimi,
                    'etunimi' => $_SESSION['kayttaja']->etunimi,
                    'sukunimi' => $_SESSION['kayttaja']->sukunimi,
                    'email' => $_SESSION['kayttaja']->email,
                    'osoite' => $this->request->getVar('osoite'),
                    'postinro' => $this->request->getVar('postinro'),
                    'kaupunki' => $this->request->getVar('kaupunki'),
                    'salasana' => $_SESSION['kayttaja']->salasana
                ];
                $this->ostoskoriModel->tilaa($asiakas);

                $userId = $_SESSION['kayttaja']->id;

                $user = $this->asiakasModel->find($userId);
                //print_r($user);
                $_SESSION['kayttaja'] = $user;

                //print_r($_SESSION['kayttaja']);
                echo view('templates/header');
                echo view('templates/nav_tilaus');
                echo view('tilaa/kiitos');
                echo view('templates/footer');
            }
        }
    }

    /**
     * Tilauksen yhteydessä rekisteröinti
     */
    function rekisterointiTilaus()
    {

        if (!$this->validate([
            'kayttajanimi' => 'required|min_length[8]|max_length[30]',
            'salasana' => 'required|min_length[8]|max_length[30]',
            'vahvistasalasana' => 'required|min_length[8]|max_length[30]|matches[salasana]',
            'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[asiakas.email]',
            'postinro' => 'required|min_length[5]|max_length[5]|numeric'
        ])) {
            echo view('templates/header');
            echo view('templates/nav_tilaus');
            echo view('tilaa/asiakas');
            echo view('templates/footer');
        } else {
            $asiakas = [
                'id' => 0,
                'kayttajanimi' => $this->request->getVar('kayttajanimi'),
                'etunimi' => $this->request->getVar('etunimi'),
                'sukunimi' => $this->request->getVar('sukunimi'),
                'email' => $this->request->getVar('email'),
                'osoite' => $this->request->getVar('osoite'),
                'postinro' => $this->request->getVar('postinro'),
                'kaupunki' => $this->request->getVar('kaupunki'),
                'salasana' => password_hash($this->request->getVar('salasana'), PASSWORD_DEFAULT)
            ];
            $this->ostoskoriModel->tilaa($asiakas);
            echo view('templates/header');
            echo view('templates/nav_tilaus');
            echo view('tilaa/kiitos');
            echo view('templates/footer');
        }
    }
}
