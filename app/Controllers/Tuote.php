<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;

class Tuote extends BaseController
{
    private $tuoteryhmaModel = null;
    private $tuoteModel = null;

    function __construct()
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->tuoteModel = new TuoteModel();

        $session = \Config\Services::session();
        $session->start();
    }

    /**
     * Näyttää tuotteet (tuoteryhmän mukaan). 
     */
    public function index($tuoteryhma_id)
    {
        // Jos tuoteryhmää ei välitetä parametrina, haetaan 1. tuoteryhmä tietokannasta ja näytetään
        // sen tuotteet.
        if ($tuoteryhma_id === null) {
            $tuoteryhma_id = $this->tuoteryhmaModel->haeEnsimmainenTuoteryhma();
        }

        // Haetaan ja aseteatan lomakkeella näytettävä tiedot muuttujiin.
        $data['tuoteryhma_id'] = $tuoteryhma_id;
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
        $data['otsikko'] = 'Tuotteet';
        echo view('templates/header');
        echo view('admin/nav_admin', $data);
        echo view('admin/tuottet_admin.php', $data);
        echo view('templates/footer.php');
    }

    /**
     * Vaihtaa post-parametrina saadun tuoteryhmän mukaiset tuotteet näkyviin. Tätä metodia
     * käytetään, kun käyttöliittymässä vaihdetaan tuoteryhmää pudotuslistassa. 
     */
    public function vaihdaRyhma()
    {
        $tuoteryhma_id = $this->request->getPost('tuoteryhma_id');
        $this->index($tuoteryhma_id);
    }

    /**
     * Tallentaa tuotteen.
     * 
     * @param int $tuoteryhma_id Tuoteryhmän id, jonka alle tuote lisätään.
     * @param int $tuote Tuotteen id, mikäli ollaan muokkaamassa tuotteen tietoja.
     */
    public function tallenna($tuoteryhma_id, $tuote_id = null)
    {
        // Asetetaan otsikko sen mukaan, ollaanko lisäämässä vai muokkaamassa tuotteen tietoja.
        if ($tuote_id != null || $this->request->getPost('id') != null) {
            $data['otsikko'] = "Muokkaa tuotetta";
        } else {
            $data['otsikko'] = "Lisää tuote";
        }

        // Tuoteryhmän id kulkee mukana lomakkeella aina, koska tuote pitää aina tallentaa jonkin 
        // tuoteryhmän alle.
        $data['tuoteryhma_id'] = $tuoteryhma_id;
        
        // Jos post-metodi, yritetään tallentaa.
        if ($this->request->getMethod() === 'post') {
            if (!$this->validate([
                'nimi' => 'required|max_length[50]',
                'hinta' => 'required'
                
            ])) {
                // Validointi ei mene läpi, palautetaan lomake näkyviin.
                $data['tuoteryhma_id'] = $tuoteryhma_id;
                $data['id'] = $this->request->getPost('id');
                $data['nimi'] = $this->request->getPost('nimi');
                $data['hinta'] = $this->request->getPost('hinta');
                $data['kuvaus'] = $this->request->getPost('kuvaus');
                $data['kunto'] = $this->request->getPost('kunto');
                $data['kuva'] = $this->request->getPost('kuva');
                $data['varastomaara'] = $this->request->getPost('varastomaara');
                $this->naytaLomake($data);

            } else {  // Tallennetaan.

                // Tallennetaan kuva ja luodaan thumbnail-samalla. Kuva ladataan, jos pystytään.
                // Tuotteen tiedot tallennetaan, vaikka kuvan lataus epäonnistuisi.
                $polku = ROOTPATH . '/public/img/tuoteryhmat/' . $tuoteryhma_id . '/';
                $tiedosto = $this->request->getFile('kuva');
                if ($tiedosto->isValid()) {

                    $tiedosto->move($polku, $tiedosto->getName());

                    \Config\Services::image()
                        ->withFile($polku . $tiedosto->getName())
                        ->fit(200, 200, 'center')
                        ->save($polku . 'thumb_' . $tiedosto->getName());
                    $talleta['kuva'] = $tiedosto->getName();
                }

                //Tallennetaan tuote tietokantaan.
                $talleta['tuoteryhma_id'] = $tuoteryhma_id;
                $talleta['id'] = $this->request->getPost('id');
                $talleta['nimi'] = $this->request->getPost('nimi');
                $talleta['hinta'] = $this->request->getPost('hinta');
                $talleta['kuvaus'] = $this->request->getPost('kuvaus');
                $talleta['kunto'] = $this->request->getPost('kunto');
                $talleta['varastomaara'] = $this->request->getPost('varastomaara');
                $this->tuoteModel->save($talleta);
                return redirect()->to(site_url('/admin/tuotteet_admin/' . $tuoteryhma_id));
            }
        } else { // Näytetään lomake.
            $data['id'] = '';
            $data['nimi'] = '';
            $data['hinta'] = 0;
            $data['kuvaus'] = '';
            $data['kuva'] = '';
            $data['kunto'] = 0;
            $data['varastomaara'] = 0;

            // Mikäli tuote on asetettu, ollaan muokkaamassa ja haetaan tietokannasta
            // tiedot lomakkeelle.
            if ($tuote_id != null) {
                $tuote = $this->tuoteModel->haeTuote($tuote_id);
                $data['id'] = $tuote['id'];
                $data['nimi'] = $tuote['nimi'];
                $data['hinta'] = $tuote['hinta'];
                $data['kuvaus'] = $tuote['kuvaus'];
                $data['kuva'] = $tuote['kuva'];
                $data['kunto'] = $tuote['kunto'];
                $data['varastomaara'] = $tuote['varastomaara'];
            }
            $this->naytaLomake($data);
        }
    }

    /**
     * Näyttää tuotteen lisäys/muokkauslomakkeen.
     *
     * @param Array $data Lomakkeelle välitettävät muuttujat taulukossa.
     */
    private function naytaLomake($data)
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        echo view('templates/header');
        echo view('admin/nav_admin', $data);
        echo view('admin/uusi.php', $data);
        echo view('templates/footer.php');
    }
}
