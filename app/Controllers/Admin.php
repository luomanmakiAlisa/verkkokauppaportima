<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\AdminTilauksetModel;
use App\Models\AdminTilausrivitModel;

class Admin extends BaseController
{
	private $tuoteryhmaModel = null;
	private $tuoteModel = null;
	private $AdminTilauksetModel = null;
	private $AdminTilausrivitModel = null;


	function __construct()
	{
		$session = \Config\Services::session();
		$session->start();

		$this->tuoteryhmaModel = new TuoteryhmaModel();
		$this->tuoteModel = new TuoteModel();
		$this->AdminTilauksetModel = new AdminTilauksetModel();
		$this->AdminTilausrivitModel = new AdminTilausrivitModel();
	}

	public function index()
	{
		if (!isset($_SESSION["admintunnus"])) {
			return redirect("adminlogin");
		}

		// $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		echo view('templates/header');
		echo view('admin/etusivu_header_admin'); //tuonne $data, jos ylempi kommentti otetaan käyttöön
		echo view('admin/etusivu_admin');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------
	public function tuotteet_admin($tuoteryhma_id = null)
	{
		if (!isset($_SESSION["admintunnus"])) {
			return redirect("adminlogin");
		}

		// Jos tuoteryhmää ei välitetä parametrina, haetaan 1. tuoteryhmä tietokannasta ja näytetään
		// sen tuotteet.
		if ($tuoteryhma_id === null) {
			$tuoteryhma_id = $this->tuoteryhmaModel->haeEnsimmainenTuoteryhma();
		}

		// Haetaan ja aseteatan lomakkeella näytettävä tiedot muuttujiin.
		$data['tuoteryhma_id'] = $tuoteryhma_id;
		$data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
		$data['tuotteet'] = $this->tuoteModel->haeTuoteryhmalla($tuoteryhma_id);
		echo view('templates/header');
		echo view('admin/nav_admin', $data);
		echo view('admin/tuotteet_admin');
		echo view('templates/footer');
	}

	public function vaihdaRyhma()
	{
		$tuoteryhma_id = $this->request->getPost('tuoteryhma_id');
		$this->tuotteet_admin($tuoteryhma_id);
	}

	public function delete($tuote_id)
	{
		$this->tuoteModel->poista($tuote_id);
		return redirect('tuotteet_admin');
	}

	public function poista($id)
	{
		// Poisto suoritetaan try-catch -lauseen sisällä, koska tuoteryhmän poistamisessa voi tapahtua virhe, mikäli tuoteryhmän tuotteita on tilattu, 
		// jolloin poista ei onnistu.
		try {
		  // Poistetaan ensin tuotteet tuoteryhmän alta.
		  $this->tuoteModel->poista($id);
		  return redirect()->to(site_url("tuotteet_admin"));
		}
		catch (\Exception $e) {
		  // Tarkastetaan, johtuuko virhe siitä, että tuoteryhmän alla oleville tuotteille on tilauksia, jolloin poistaminen
		  // ei onnistu.
		  if ($e->getCode() === 1451) {	
			echo view('templates/header');
			echo view('admin/nav_admin');  
			echo view('admin/tuote_1451_virhe.php');
			echo view('templates/footer.php');
		  }
		  else { // Heitetään poikkeus edelleen, mikä aiheuttaa virheen näyttämisen.
			throw new $e;
		  }
		}
	}

	// Tilaukset

	public function tilaukset_admin()
	{

		if (!isset($_SESSION["admintunnus"])) {
			return redirect("adminlogin");
		}
		$data['tilaukset'] = $this->AdminTilauksetModel->haeTilaukset();
		echo view('templates/header');
		echo view('admin/nav_admin');
		echo view('admin/tilaukset_admin', $data);
		echo view('templates/footer');
	}

	public function tilausrivit_admin($id)
	{
		if (!isset($_SESSION["admintunnus"])) {
			return redirect("adminlogin");
		}
		$data['adminTilausrivit'] = $this->AdminTilausrivitModel->katsoTilaus($id);
		//var_dump($data['adminTilausrivit']);
		//exit;
		echo view('templates/header');
		echo view('admin/nav_admin');
		echo view('admin/tilausrivit_admin', $data);
		echo view('templates/footer');
	}


		/* Tuoteryhma lisäys ja poisto */

		public function tuoteryhmat_admin()
		{
			if(!isset($_SESSION["admintunnus"]))
			{
				return redirect("adminlogin");
			}
			
			$data["tuoteryhmat"] = $this->tuoteryhmaModel->haeTuoteryhmat();
	
			echo view('templates/header');
			echo view('admin/nav_admin');
			echo view('admin/tuoteryhmat_admin', $data);
			echo view('templates/footer');
		}
	
		public function luo_tr() {
			if(!isset($_SESSION["admintunnus"]))
			{
				return redirect("adminlogin");
			}
	
			$model = new TuoteryhmaModel();
	
			if(!$this->validate([
				"uusi_ryhma" => "required|max_length[50]",
			])) {
				echo view('templates/header');
				echo view('admin/nav_admin');
				echo view('admin/uusi_ryhma');
				echo view('templates/footer');
			}
			else {
				try {
				$this->tuoteryhmaModel->save([
					"nimi" => $this->request->getVar("uusi_ryhma")
				]);
				return redirect()->to(site_url("tuoteryhmat_admin"));				
				}
				catch (\Exception $e) {
					if ($e->getCode() === 1062) {
						echo view('templates/header');
						echo view('admin/nav_admin');
						echo view('admin/tr_1062_virhe');
						echo view('templates/footer');
					}
					else {
						throw new $e;
					}
				}
			}
		}
	
		public function poista_tr($id)
		{
			try {
				$this->tuoteryhmaModel->poista_tr($id);
				return redirect()->to(site_url("tuoteryhmat_admin"));		   
			}
			catch (\Exception $e) {
				if ($e->getCode() === 1451) {
					echo view('templates/header');
					echo view('admin/nav_admin');
					echo view('admin/tr_1451_virhe');
					echo view('templates/footer');
				}
				else {
					throw new $e;
				}
			}
		}
}
