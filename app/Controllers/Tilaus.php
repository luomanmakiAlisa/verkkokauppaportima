<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\OstoskoriModel;

class Tilaus extends BaseController
{

    function __construct()
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->ostoskoriModel = new OstoskoriModel();

        $session = \Config\Services::session();
        $session->start();
    }

    public function index()
    {
    }
}
