<?php

namespace App\Controllers;

use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;
use App\Models\OstoskoriModel;

/**
 * Verkkokauppa-toiminnallisuuden käsittelijä.
 */
class Kauppa extends BaseController
{
    private $tuoteryhmaModel = null;
    private $tuoteModel = null;
    private $ostoskoriModel = null;

    function __construct()
    {
        $this->tuoteryhmaModel = new TuoteryhmaModel();
        $this->tuoteModel = new TuoteModel();
        $this->ostoskoriModel = new OstoskoriModel();

        $session = \Config\Services::session();
        $session->start();
    }


    /**
     * Näyttää tuoteryhmän mukaiset tuotteet.
     */
    public function index($tuoteryhma_id)
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['tuotteet'] = $this->tuoteModel->haeTuoteRyhmalla($tuoteryhma_id);
        $data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
        $data['otsikko'] = $this->tuoteryhmaModel->hae($tuoteryhma_id)['nimi'];
        if (isset($_SESSION['kayttaja'])) {
            $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
        }
        echo view('templates/header');
        echo view('templates/nav', $data);
        echo view('kauppa', $data);
        echo view('templates/footer');
    }


    /**
     * Näyttää yksittäisen tuotteen.
     */
    public function tuote($tuote_id)
    {
        $data['tuoteryhmat'] = $this->tuoteryhmaModel->haeTuoteryhmat();
        $data['tuote'] = $this->tuoteModel->haeTuote($tuote_id);
        $data['yhteisSumma'] = $this->ostoskoriModel->yhteisSumma();
        if (isset($_SESSION['kayttaja'])) {
            $data['etunimi'] = $_SESSION['kayttaja']->etunimi;
        }
        echo view('templates/header');
        echo view('templates/nav', $data);
        echo view('tuote', $data);
        echo view('templates/footer');
    }

    //--------------------------------------------------------------------

}
