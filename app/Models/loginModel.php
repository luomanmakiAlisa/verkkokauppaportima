<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
    protected $table = 'asiakas';

    protected $allowedFields = ['kayttajanimi', 'etunimi', 'sukunimi', 'osoite', 'postinro', 'kaupunki', 'email', 'salasana'];

    public function check($kayttajanimi, $salasana)
    {
        $this->where('kayttajanimi', $kayttajanimi);
        $query = $this->get();
        $row = $query->getRow(); //hae rivi
        if ($row) { // tarkista onko käyttäjä
            if (password_verify($salasana, $row->salasana)) { // tarkistaa onko salasana oikein 
                return $row; // salasana oli oikein. palauta käyttäjä
            }
        }
        return null; // tunnus tai salasana väärin
    }
}
