<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminTilauksetModel extends Model
{
    protected $table = 'tilaus';
    protected $allowedFields = ['paivays', 'asiakas_id'];


    public function haeTilaukset()
        { 
          $this->select('tilaus.id, asiakas.etunimi, asiakas.sukunimi, tilaus.paivays');
          $this->join('asiakas', 'asiakas.id = tilaus.asiakas_id');
          return $this->findAll();
        }
}