<?php

namespace App\Models;

use CodeIgniter\Model;

class TuoteryhmaModel extends Model
{
  protected $table = 'tuoteryhma';
  protected $allowedFields = ['nimi','id'];

  public function haeTuoteryhmat()
  {
    return $this->findAll();
  }

  /**
   * Hakee tuoteryhmän
   * 
   * @param int $id Haettavan tuotteen id.
   * @return Array Haetun tuoteryhmän tiedot taulukkona (yksi rivi).
   */
  public function hae($id)
  {
    return $this->getWhere(['id' => $id])->getRowArray();
  }

  /**
   * Hakee ensimmäisen tuoteryhmän tietokannassa.
   */
  public function haeEnsimmainenTuoteryhma()
  {
    $this->select('id');
    $this->orderBy('id', 'asc');
    $this->limit(1);
    $query = $this->get();
    $tuoteryhma = $query->getRowArray();
    return $tuoteryhma['id'];
  }

  public function poista_tr($id)
  {
    $this->where('id',$id);
    $this->delete();
  }
}
