<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminLoginModel extends Model
{
    protected $table = 'yllapitaja';

    protected $allowedFields = ['admintunnus', 'etunimi', 'sukunimi', 'salasana'];

    public function admin_check($admintunnus, $salasana)
    {
        $this->where('admintunnus', $admintunnus);
        $query = $this->get();
        $row = $query->getRow();
        if ($row) {
            if (password_verify($salasana, $row->salasana)) { 
                return $row;
            }
        }
        return null;
    }
}