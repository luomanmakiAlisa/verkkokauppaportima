<?php

namespace App\Models;

use CodeIgniter\Model;

class OstoskoriModel extends Model
{
    private $tuoteModel = null;
    private $asiakasModel = null;
    private $tilausModel = null;
    private $tilausriviModel = null;

    function __construct()
    {
        // Istunto päälle ja luodaan tarvittaessa uusi ostoskori.
        $session = \Config\Services::session();
        $session->start();
        if (!isset($_SESSION['kori'])) {
            $_SESSION['kori'] = array();
        }

        // Otetaan viittaus tietokanta-olion, jotta voidaan hallinnoida transaktoita.
        $this->db = \Config\Database::connect();

        // Luodaan oliot tarvittavista malleista.
        $this->tuoteModel = new TuoteModel();
        $this->asiakasModel = new AsiakasModel();
        $this->tilausModel = new TilausModel();
        $this->tilausriviModel = new TilausriviModel();
    }

    public function ostoskori()
    {
        return $_SESSION['kori'];
    }

    public function lisaa($tuote_id)
    {
        // Haetaan tuote id:n perusteella tietokannasta tuoteModelia kautta.
        $tuote = $this->tuoteModel->haeTuote($tuote_id);

        // Luodaan ostoskoriin lisättävä tuote (taulukko, jossa kenttinä tarvittavat tiedot.)
        $ostoskorinTuote['id'] = $tuote['id'];
        $ostoskorinTuote['nimi'] = $tuote['nimi'];
        $ostoskorinTuote['hinta'] = $tuote['hinta'];
        $ostoskorinTuote['tuoteryhma_id'] = $tuote['tuoteryhma_id'];
        $ostoskorinTuote['maara'] = 1;

        // Kutsutaan private-metodia, joka lisää tuotteen taulukkoon tai lisää määrää, jos tuote oli
        // jo taulukossa
        $this->lisaaTuoteTaulukkoon($ostoskorinTuote, $_SESSION['kori']);
    }

    public function lukumaara()
    {
        return count($_SESSION['kori']);
    }

    private function lisaaTuoteTaulukkoon($tuote, &$taulukko)
    {
        // Käydään läpi taulukon rivit.
        for ($i = 0; $i < count($taulukko); $i++) {
            // Jos löytyy id:llä, tuote on jo taulukossa. Päivitetään määrää.
            if ($taulukko[$i]['id'] === $tuote['id']) {
                $taulukko[$i]['maara'] = $taulukko[$i]['maara']  + 1;
                return; // Koska tuote voi olla vain kerran taulukossa, voidaan for-lause ja etsiminen
                // lopettaa, jos tuote löytyy. Return-lauseen avulla poistutaan for-lauseesta.
            }
        }
        $tuote['maara'] = 1; // Tuote ei ollut taulukossa, joten asetetaan määräksi 1.
        array_push($taulukko, $tuote);
    }

    /*Laskee ostoskorin yhteissumman ja palauttaa sen*/
    public function yhteisSumma()
    {
        $summa = 0;
        $tuotteet = $_SESSION['kori'];
        foreach ($tuotteet as $ostos) {
            $summa += $ostos['hinta'] * $ostos['maara'];
        }
        return $summa;
    }

    //** poistaa yhden tuotteen korista. Jos kaikki tuotteet poistetaan niin rivi poistuu korista*/
    public function poista($tuote_id)
    {
        // Käydään ostoskori läpi ja jos id:llä löytyy tuote, poistetaan yksi tuote korista.
        for ($i = count($_SESSION['kori']) - 1; $i >= 0; $i--) {
            $tuote = $_SESSION['kori'][$i];
            if ($tuote['id'] === $tuote_id) {
                $_SESSION['kori'][$i]['maara'] = $_SESSION['kori'][$i]['maara']  - 1;
                if ($_SESSION['kori'][$i]['maara'] <= 0) {
                    array_splice($_SESSION['kori'], $i, 1);
                }
            }
        }
    }

    //** Poistaa ostoskorista koko tuote rivin*/
    public function poistaTuote($tuote_id)
    {
        // Käydään ostoskori läpi ja jos id:llä löytyy tuote, poistetaan yksi tuote korista.
        for ($i = count($_SESSION['kori']) - 1; $i >= 0; $i--) {
            $tuote = $_SESSION['kori'][$i];
            if ($tuote['id'] === $tuote_id) {
                array_splice($_SESSION['kori'], $i, 1);
            }
        }
    }

    /**
     * Tyhjentää ja luo uuden tyhjän ostoskorin.
     */
    public function tyhjenna()
    {
        $_SESSION['kori'] = null;
        $_SESSION['kori'] = array();
    }

    /**
     * Tallentaa tilauksen tietokantaan (asiakas, tilaus ja tilausrivi).
     * 
     * @param Array $asiakas Asiakkaan tiedot.
     */
    public function tilaa($asiakas)
    {

        // Aloitetaan transaktiot.
        $this->db->transStart();

        //Tallennetaan asiakas);
        if ($asiakas['id'] == 0) {
            $this->asiakasModel->save($asiakas);
            $asiakas_id = $this->insertID();
        } else {
            $this->asiakasModel->save($asiakas);
            $asiakas_id = $asiakas['id'];
        }
        // Tallennetaan tilaus.
        $this->tilausModel->save(['asiakas_id' => $asiakas_id]);
        $tilaus_id = $this->insertID();

        // Tallennetaan tilausrivit.
        foreach ($_SESSION['kori'] as $tuote) {
            $this->tilausriviModel->save([
                'tilaus_id' => $tilaus_id,
                'tuote_id' => $tuote['id'],
                'maara' => $tuote['maara']
            ]);
        }
        // Ostoskori tyhjennetään onnistuneen tilauksen jälkeen.
        $this->tyhjenna();
        // Päätetään transaktio.
        $this->db->transComplete();
    }
}
