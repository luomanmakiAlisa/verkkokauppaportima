<?php

namespace App\Models;

use CodeIgniter\Model;

class AsiakasModel extends Model
{
    protected $table = 'asiakas'; // Asiakas taulu tietokannasta

    //Sallitut tallennus arvot tietokantaan.
    protected $allowedFields = ['kayttajanimi', 'etunimi', 'sukunimi', 'osoite', 'postinro', 'kaupunki', 'email', 'salasana'];


    protected $returnType = "object";
}
