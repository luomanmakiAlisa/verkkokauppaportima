<?php

namespace App\Models;

use CodeIgniter\Model;

class TilausModel extends Model
{
    protected $table = 'tilaus';

    protected $allowedFields = ['asiakas_id'];
}
