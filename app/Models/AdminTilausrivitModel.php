<?php

namespace App\Models;

use CodeIgniter\Model;

class AdminTilausrivitModel extends Model
{
    protected $table = 'tilausrivi';
    protected $allowedFields = ['tilaus_id', 'tuote_id', 'maara'];


    public function katsoTilaus($id)
    { 
        $this->select('tuote.nimi, tilausrivi.maara');
        $this->join('tuote', 'tuote.id = tilausrivi.tuote_id');
        return $this->getWhere(['tilaus_id' => $id])->getResultArray();
    }
}
