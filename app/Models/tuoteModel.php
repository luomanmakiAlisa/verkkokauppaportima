<?php

namespace App\Models;

use CodeIgniter\Model;

class TuoteModel extends Model
{
  protected $table = 'tuote';

  protected $allowedFields = ['nimi','hinta','kuvaus','kuva','kunto','varastomaara','tuoteryhma_id'];

  public function haeTuoteryhmalla($tuoteryhma_id)
  {
    return $this->getWhere(['tuoteryhma_id' => $tuoteryhma_id])->getResultArray();
  }

  public function haeTuote($id)
  {
    $this->where('id', $id);
    $query = $this->get();
    $tuote = $query->getRowArray();
    return $tuote;
  }

  public function haeTuotteet($idt) {
    $palautus = array();
    foreach ($idt as $id) {
      $this->table('tuote');
      $this->select('id,nimi,hinta');
      $this->where('id',$id);
      $query = $this->get();
      $tuote = $query->getRowArray();
      array_push($palautus,$tuote);
        
      $this->resetQuery();
    }
   
    return $palautus;
  }

  public function poista($id) {
    $this->where('id',$id);
    $this->delete();
  }
  
}
