<h3>Rekisteröidy käyttäjäksi</h3>
<!--
<p>Jos tiliä ei ole ole. Jatka rekisteröintiin tai kirjaudu sisään</p>
<div class="row">
    <a href="#" type="button" class="btn btn-secondary">Kirjaudu sisään</a>
</div>
-->
<p>Täytä alla olevat tiedot ja jatka.</p>

<div>
    <h4>Tilaajan tiedot</h4>
    <form action="/ostoskori/rekisterointiTilaus">
        <div class="col-12">
            <?= Config\Services::validation()->listErrors(); ?>
        </div>
        <div class="form-group">
            <label>Käyttäjänimi</label>
            <div class="form-group">
                <input class="form-control" name="kayttajanimi" maxlength="30">
            </div>
            <label>Etunimi</label>
            <input name="etunimi" maxlength="50" class="form-control">
        </div>
        <div class="form-group">
            <label>Sukunimi</label>
            <input name="sukunimi" maxlength="100" class="form-control">
        </div>
        <div class="form-group">
            <label>Lähiosoite</label>
            <input name="osoite" maxlength="100" class="form-control">
        </div>
        <div class="form-group">
            <label>Postinumero</label>
            <input name="postinro" maxlength="5" class="form-control">
        </div>
        <div class="form-group">
            <label>Postitoimipaikka</label>
            <input name="kaupunki" maxlength="100" class="form-control">
        </div>
        <div class="form-group">
            <label>Sähköposti</label>
            <input name="email" type="email" maxlength="255" class="form-control">
        </div>
        <div class="form-group">
            <label>Salasana</label>
            <input class="form-control" name="salasana" type="password" maxlength="30">
        </div>
        <div class="form-group">
            <label>Salasana uudelleen</label>
            <input class="form-control" name="vahvistasalasana" type="password" maxlength="30">
        </div>
        <!--<div class="form-group">
            <label>Puhelin</label>
            <input name="puhelin" maxlength="20" class="form-control">
        </div>-->
        <div class="row">
            <button class="btn btn-secondary btn-lg btn-block">Siirry kassalle</button>
        </div>
    </form>
</div>