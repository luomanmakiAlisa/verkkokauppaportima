<h3>Tervetuloa tilamaan <?= $etunimi ?>!</h3>

<p>Varmista alla olevat yhteystiedot oikeiksi.</p>

<div>
    <h4>Tilaajan tiedot</h4>
    <form action="<?= site_url('ostoskori/tilaa') ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Lähiosoite</label>
            <input name="osoite" maxlength="100" class="form-control" value="<?= $osoite ?>">
        </div>
        <div class="form-group">
            <label>Postinumero</label>
            <input name="postinro" maxlength="5" class="form-control" value="<?= $postinro ?>">
        </div>
        <div class="form-group">
            <label>Postitoimipaikka</label>
            <input name="kaupunki" maxlength="100" class="form-control" value="<?= $kaupunki ?>">
        </div>
        <div class="row">
            <button class="btn btn-secondary btn-lg btn-block">Siirry kassalle</button>
        </div>
    </form>
</div>