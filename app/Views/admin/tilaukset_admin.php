<div class="text-center">
    <h3 class="m-4">Tilaukset</h3>
</div>

<table class="table mt-4">
    <tr>
        <th>Tilausnumero</th>
        <th>Asiakas</th>
        <th>Päiväys</th>
    </tr>
    
    <?php foreach ($tilaukset as $tilaus) : ?>
        <tr>
            <td><?= $tilaus['id'] ?></td>
            <td><?= $tilaus['etunimi'] ?>&nbsp;<?= $tilaus['sukunimi']?></td>
            <td><?= $tilaus['paivays'] ?></td>
            <form action="<?= base_url('/admin/tilausrivit_admin/' . $tilaus['id']) ?>" method="post" >
            <td><button class="btn btn-secondary btn-sm" type="submit">Näytä tilaus</button></td>
            </form>
        </tr>
    <?php endforeach; ?>

</table>