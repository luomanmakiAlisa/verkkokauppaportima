<div class="text-center">
    <h3 class="m-4">Tilaus</h3>
</div>

<table class="table mt-4">
    <tr>
        <th>Tuote</th>
        <th>Määrä</th>
    </tr>
    
    <?php foreach ($adminTilausrivit as $tilausrivi) : ?>
        <tr>
            <td><?= $tilausrivi['nimi'] ?></td>
            <td><?= $tilausrivi['maara'] ?></td>
        </tr>
    <?php endforeach; ?>

</table>

<div class="text-center">
<a class="link" href="<?= site_url('/admin/tilaukset_admin') ?>">Takaisin tilauksiin</a>
</div>