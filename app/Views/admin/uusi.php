<div class="row">
    <h3 class="m-5">Lisää uusi tuote</h3>
</div>
<div class="row">
    <div class="col-12">
        <form action="/tuote/tallenna/<?= $tuoteryhma_id ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?= $id ?>">
            <div class="col-12">
                <?= Config\Services::validation()->listErrors(); ?>
            </div>
            <div class="form-row ml-4">
                <div class="form-group ml-1">
                    <label>Tuotteen nimi</label>
                    <input class="form-control" name="nimi" placeholder="Nimi" maxlength="255" value="<?= $nimi ?>" />
                </div>
                <div class="form-group ml-4">
                    <label>Hinta</label>
                    <input class="form-control" name="hinta" placeholder="0.00" maxlength="5" value="<?= $hinta ?>" />
                </div>
                <div class="form-group ml-4">
                    <label>Varastomäärä</label>
                    <input class="form-control" name="varastomaara" placeholder="kpl" maxlength="3" value="<?= $varastomaara ?>" />
                </div>
                <div class="form-group ml-4">
                    <label>Kunto 1 - 5</label> <br>
                    <select name="kunto">
                        <!-- Tämmöinen pikaratkaisukoodi :D anteeksi - alisa -->
                        <?php
                        if ($kunto == 5) {
                            print "<option value='1'>1 tähti</option>";
                            print "<option value='2'>2 tähteä</option>";
                            print "<option value='3'>3 tähteä</option>";
                            print "<option value='4'>4 tähteä</option>";
                            print "<option value='5' selected>5 tähteä</option>";
                        } else if ($kunto == 4) {
                            print "<option value='1'>1 tähti</option>";
                            print "<option value='2'>2 tähteä</option>";
                            print "<option value='3'>3 tähteä</option>";
                            print "<option value='4' selected>4 tähteä</option>";
                            print "<option value='5'>5 tähteä</option>";
                        } else if ($kunto == 3) {
                            print "<option value='1'>1 tähti</option>";
                            print "<option value='2'>2 tähteä</option>";
                            print "<option value='3' selected>3 tähteä</option>";
                            print "<option value='4'>4 tähteä</option>";
                            print "<option value='5'>5 tähteä</option>";
                        } else if ($kunto == 2) {
                            print "<option value='1'>1 tähti</option>";
                            print "<option value='2'selected>2 tähteä</option>";
                            print "<option value='3'>3 tähteä</option>";
                            print "<option value='4'>4 tähteä</option>";
                            print "<option value='5'>5 tähteä</option>";
                        } else {
                            print "<option value='1' selected>1 tähti</option>";
                            print "<option value='2'>2 tähteä</option>";
                            print "<option value='3'>3 tähteä</option>";
                            print "<option value='4'>4 tähteä</option>";
                            print "<option value='5'>5 tähteä</option>";
                        }

                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group ml-4">
                <label>Kuvaus</label>
                <textarea class="form-control" name="kuvaus" placeholder="Tuotteen kuvaus ja tiedot" rows="6" /><?= $kuvaus ?></textarea>
            </div>

            <div class="form-group ml-4">
                <label for="file">Kuva</label>
                <input type="file" class="form-control" id="kuva" name="kuva">
                <?php
                if ($kuva !== '') {
                    $polku = base_url('img/tuoteryhmat/' . $tuoteryhma_id . '/' . $kuva);
                    print "<img class='col-2' src='" . $polku  . "'/>";
                }
                ?>

            </div>

            <button class="btn btn-admin ml-4 mb-5 mt-5">Tallenna</button>

        </form>
    </div>
</div>