<h3>Tuoteryhmät</h3>
<div>
    <h5><?= anchor("luo_tr", "Lisää uusi tuoteryhmä") ?></h5>
</div>
<table class="table">
    <tr>
        <th>TR-numero</th>
        <th>Tuoteryhmä</th>
        <th><i class="far fa-trash-alt"></i></th>
    </tr>
    <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
        <tr>
            <td><?= $tuoteryhma["id"] ?></td>
            <td><?= $tuoteryhma["nimi"] ?></td>
            <td><a href="<?= site_url('admin/poista_tr/' . $tuoteryhma['id']) ?>" onclick="return confirm('Haluatko todella poistaa tuoteryhmän?')">Poista</a></td>
        </tr>
    <?php endforeach; ?>
</table>
<form action="/admin">
    <button class="btn btn-admin m-1">Takaisin</button>
</form>