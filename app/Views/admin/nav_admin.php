<body>
    <header>
        <div class="jumbotron rounded-0">
            <div class="row col-12">
                <div class="col-6 d-flex justify-content-start">
                    <img src="<?= base_url('/../img/logo_icons/logo_valkoinen.png') ?>" height="80">
                </div>

            </div>
        </div>
        <div class="d-flex justify-content-center navTuoteryhma">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('admin') ?>">Etusivu</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('/admin/tuotteet_admin') ?>">Tuotteet</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('/admin/tuoteryhmat_admin') ?>">Tuoteryhmät</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('/admin/tilaukset_admin') ?>">Tilaukset</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/adminlogin/logout">Kirjaudu ulos</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <div class="container">