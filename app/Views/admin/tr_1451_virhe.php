<h3>Tuoteryhmää ei voida poistaa.</h3>
<p>Poista ensin kaikki tuoteryhmän sisältämät tuotteet.</p>
<form action="/admin/tuoteryhmat_admin">
    <button class="btn btn-admin m-1">Takaisin</button>
</form>