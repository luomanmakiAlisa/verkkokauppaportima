<h3>Lisää tuoteryhmä</h3>
<form action="luo_tr">
    <div>
        <?= Config\Services::validation()->listErrors(); ?>
        <div>
            <label>Tuoteryhmä</label>
            <input class="form-control" name="uusi_ryhma" placeholder="Lisää tuoteryhmä" maxlength="50">
        </div>
    </div>
    <button class="btn btn-admin m-1">Tallenna</button>
</form>
<form action="tuoteryhmat_admin">
    <button class="btn btn-admin m-1">Takaisin</button>
</form>