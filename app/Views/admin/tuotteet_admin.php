<div class="text-center">
    <h3 class="m-4">Tuotteet</h3>
</div>
<form action="/admin/vaihdaRyhma/" method="post">
    <label class="ml-4">Tuoteryhmä</label>
    <select name="tuoteryhma_id" onChange="this.form.submit()">
        <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
            <option value="<?= $tuoteryhma['id'] ?>" <?php
                                                        // Asetetaan tuoteryhmä valituksi pudotuslistassa kirjoittamalla selected html:n sekaan oikeaan kohtaan.
                                                        if ($tuoteryhma['id'] === $tuoteryhma_id) {
                                                            print " selected";
                                                        } ?>>
                <?= $tuoteryhma['nimi'] ?>
            </option>
        <?php endforeach; ?>
    </select>
</form>

<span class="ml-5"><?= anchor('tuote/tallenna/'  . $tuoteryhma_id, 'Lisää uusi tuote') ?></span>

<table class="table mt-4">
    <tr>
        <th>Kuva</th>
        <th>Tuotenimi</th>
        <th>Hinta</th>
        <th>Kuvaus</th>
        <th>Kappalemäärä</th>
        <th>Kunto (1 -5)</th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($tuotteet as $tuote) : ?>
        <tr>
            <td><img class="pikkukuva" src="<?= base_url(['img/tuoteryhmat/' . $tuote['tuoteryhma_id'] . '/' . $tuote['kuva']]) ?>"></img></td>
            <td><?= $tuote['nimi'] ?></td>
            <td><?= $tuote['hinta'] ?></td>
            <td><?= $tuote['kuvaus'] ?></td>
            <td><?= $tuote['varastomaara'] ?></td>
            <td><?= $tuote['kunto'] ?></td>
            <td><?= anchor('tuote/tallenna/' . $tuoteryhma_id . '/' . $tuote['id'], 'Muokkaa') ?></td>
            <td><a href="<?= site_url('admin/poista/' . $tuote['id']) ?>" onclick="return confirm('Haluatko varmasti poistaa tuotteen?')">Poista</a></td>

        </tr>
    <?php endforeach; ?>
</table>