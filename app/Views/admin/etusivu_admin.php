<div class="row adminEtusivuRow">
    <div class="col-12 col-md-5 mt-4 mb-4 mt-md-4 mb-md-4 p-4 adminEtusivuDiv">
        <div>
            <i class="fas fa-user-cog"></i>
        </div>
        <div>
            <h4>Tervetuloa</h4>
            <form action="/admin/tuoteryhmat_admin">
                <button class="btn btn-admin m-1">Tuoteryhmät</button>
            </form>
            <form action="/admin/tuotteet_admin">
                <button class="btn btn-admin m-1">Tuotteet</button>
            </form>
            <form action="/admin/tilaukset_admin">
                <button class="btn btn-admin m-1">Tilaukset</button>
            </form>
        </div>
    </div>
    <div class="col-12 col-md-5 mb-4 mt-md-4 mb-md-4 p-4 adminEtusivuDiv">
        <div>
            <i class="fas fa-users-cog"></i>
        </div>
        <div>
            <h4>Tili</h4>
        </div>
        <form action="adminlogin/admin_register">
            <button class="btn btn-admin m-1">Lisää uusi tili</button>
        </form>
        <form action="#">
            <button class="btn btn-admin m-1">Päivitä tiliä</button>
        </form>
        <form action="/adminlogin/logout">
            <button class="btn btn-admin m-1">Kirjaudu ulos</button>
        </form>
    </div>
</div>