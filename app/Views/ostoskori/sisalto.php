<div class="row">
    <div class="col">
        <h3>Ostoskori</h3>
        <a href="<?= site_url('ostoskori/tyhjenna/') ?>" class="roskis">
            Tyhjennä ostoskori<i class="fas fa-trash-alt"></i></a>
        <table class="table table-borderless koriTable">
            <?php
            $summa = 0;
            ?>
            <tr class="viivaaOstoskori">
                <td>
                    Tuote
                </td>
                <td>
                    Hinta/Kpl
                </td>
                <td>
                    Määrä
                </td>
                <td class="text-right">
                    Hinta
                </td>
                <td class="text-right">
                    Poista
                </td>
            </tr>
            <?php
            if (count($tuotteet) == 0) {
                echo  "<tr style='height: 50px;'><td colspan='4'>";
                echo  "<tr style='height: 200px;'><td colspan='4'>";
                print '<p class="text-center"><i class="fas fa-shopping-bag"></i></i></p>';
                echo  "</td></tr>";
                echo  "<tr style='height: 50px;'><td colspan='4'>";
            }; ?>
            <?php foreach ($tuotteet as $tuote) : ?>
                <tr class="viivaaOstoskori">
                    <td>
                        <?= $tuote['nimi'] ?>
                    </td>
                    <td>
                        <?= $tuote['hinta'] . ' €' ?>
                    </td>
                    <td>
                        <a class="ostoskori_poista" href="<?= site_url('ostoskori/ostoskori_poista/' . $tuote['id']) ?>">
                            <i class="fas fa-minus"></i>
                        </a>
                        <?= $tuote['maara'] ?>
                        <a class="ostoskori_lisaa" href="<?= site_url('ostoskori/ostoskori_lisaa/' . $tuote['id']) ?>">
                            <i class="fas fa-plus"></i>
                        </a>
                    </td>
                    <td>
                        <p class="hintaYhteensa text-right">
                            <span>
                                <?php
                                $summaYhteensa = $tuote['hinta'] * $tuote['maara'];
                                printf("%.2f ", $summaYhteensa); ?></span>
                            €<p>
                    </td>
                    <td class="text-right">
                        <a href="<?= site_url('ostoskori/poistaTuote/' . $tuote['id']) ?>" class="roskis">
                            <i class="fas fa-trash-alt" style="margin-right: 10px;"></i></a>
                    </td>
                </tr>
                <?php
                $summa += $tuote['hinta'] * $tuote['maara'];
                ?>
            <?php endforeach; ?>
            <tr class="viivaaOstoskori">
                <td>
                    <i class="fas fa-truck"></i>
                </td>
                <td>
                    Postitoimikulut
                </td>
                <td>
                </td>
                <td class="text-right">
                    <?php
                    if (count($tuotteet) == 0) {
                        print "--.- €";
                    } else {
                        print "6.90 €";
                    }; ?>
                </td>
                <td>
                </td>
            </tr>
            <tr class="viivaaOstoskoriPaksu">
                <td colspan='4'>
                    <p class="ostoksetYhteensa text-right">Yhteensä
                        <span style="color:#ff00a0;">
                            <?php
                            if ($summa !== 0) {
                                $summa = 6.90 + $summa;
                            }
                            printf("%.2f ", $summa); ?></span>
                        €<p>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="row">
    <?php
    if (count($tuotteet) != 0) : ?>

        <a href='<?= site_url('ostoskori/asiakastiedot') ?>' type=' button' class='btn btn-secondary btn-lg btn-block'>Tilaa</a>

    <?php endif; ?>
</div>