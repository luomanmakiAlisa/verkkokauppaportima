<body>
    <header>
        <nav class="navbar navbar-expand-lg fixed-top" id="toolbar">

            <button class="navbar-toggler" data-toggle="collapse" data-target="#toolbar-collapse">
                <span>
                    <i class="fas fa-bars"></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="toolbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="d-flex navigation">
                            <span class="nav-link" id="scrollUp">
                                <i class="fas fa-arrow-circle-up"></i>
                            </span>
                            <a class="nav-link" href="<?= site_url('/') ?>">
                                <i class="fas fa-home"></i>
                            </a>
                            <span class="nav-link" id="scrollDown">
                                <i class="fas fa-arrow-circle-down"></i>
                            </span>
                            <a class="nav-link" href="<?= site_url('login/index') ?>">
                                <i class="fas fa-user-alt"></i>
                            </a>
                            <?php
                            if (isset($_SESSION['kayttaja'])) : ?>
                                <a class="nav-link" href="<?= site_url('login/kirjaudu_ulos'); ?>">
                                    <i class="fas fa-sign-out-alt"></i>
                                </a>
                            <?php endif; ?>
                            <a class="nav-link" href="<?= site_url('ostoskori/index'); ?>">
                                <i class="fas fa-shopping-basket" id="toolbar_basket"></i>
                            </a>
                            <div id="mode_div">
                                <input type="checkbox" class="mode_input" id="mode" />
                                <label for="mode" class="mode_label" id="mode_label">
                                    <i class="fas fa-moon"></i>
                                    <i class="fas fa-sun"></i>
                                    <div class="ball" id="ball"></div>
                                </label>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item dropdown navLinkki toolbar-nav-item">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" data-target="dropdown_target" id="toolbar-dropdown-toggle" href="#">TUOTTEET</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown_target" id="dropdown-menu">
                            <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
                                <a class="dropdown-item toolbar-item" href="<?= site_url('kauppa/index/' . $tuoteryhma['id']) ?>"><?= $tuoteryhma['nimi'] ?></a>
                            <?php endforeach; ?>
                        </div>
                    </li>
                    <li class="nav-item toolbar-nav-item">
                        <a class="nav-link navLinkki" href="<?= site_url('yhteystiedot') ?>">YHTEYSTIEDOT</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron rounded-0">
            <div class="row col-12">
                <div class="col-6 d-flex justify-content-start piilotusMobiili">
                    <a href="<?= site_url('home/index/') ?>">
                        <img src="<?= base_url('/../img/logo_icons/logo_valkoinen.png') ?>" height="100"></a>
                </div>
                <!--<div class="col-6 d-flex justify-content-start vainMobiili">
                    <a href="<?= site_url('home/index/') ?>">
                        <img src="<?= base_url('/../img/logo_icons/logo_valkoinen_icon.png') ?>" height="50"></a>
                </div>-->
                <div class="piilotusMobiili row col-6 d-flex justify-content-end">
                    <div>
                        <div class="dropdown">
                            <button class="btn btn-dark kehysMusta dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                <?php
                                if (isset($_SESSION['kayttaja'])) : ?>
                                    <p class="tekstiKayttaja" style="display:inline">Hei, <?= $etunimi ?>!</p>

                                <?php endif; ?>
                                <?php
                                if (!isset($_SESSION['kayttaja'])) : ?>
                                    <p class="tekstiKayttaja" style="display:inline">Tervetuloa</p>

                                <?php endif; ?>
                                <i class="far fa-user"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <?php
                                if (isset($_SESSION['kayttaja'])) : ?>
                                    <a class="dropdown-item  toolbar-item" href="<?= site_url('asiakas/index') ?>">Katso tilisi tietoja</a>
                                    <a class="dropdown-item toolbar-item" href="<?= site_url('login/kirjaudu_ulos'); ?>">Kirjaudu ulos</a>
                                <?php endif; ?>
                                <?php
                                if (!isset($_SESSION['kayttaja'])) : ?>
                                    <a class="dropdown-item toolbar-item" href="<?= site_url('asiakas/index') ?>">Kirjaudu sisään</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div>
                        <a class=" kehys navOstoskarry" href="<?= site_url('ostoskori/index'); ?>" role="button">
                            <i class="fas fa-shopping-cart"></i>
                            <span class="navSumma"><?= $yhteisSumma ?> €</span>
                            <!-- <? printf("%.2f €", $yhteisSumma);?> -->
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center navTuoteryhma piilotusMobiili" id="navTuoteryhma">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <?php foreach ($tuoteryhmat as $tuoteryhma) : ?>
                            <a class="dropdown-item" href="<?= site_url('kauppa/index/' . $tuoteryhma['id']) ?>"><?= $tuoteryhma['nimi'] ?></a>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <div class="container">