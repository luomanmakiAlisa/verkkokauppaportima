<!-- Tässä on tälläistä testi sisältöä, ja fonttia mitä voidaan käyttää. -Alisa -->
<h1>h1. Otsikoita</h1>
<h2>h2. Otsikoita</h2>
<h3>h3. Otsikoita</h3>
<h4>h4. Otsikoita</h4>
<h5>h5. Otsikoita</h5>
<h6>h6. Otsikoita</h6>

<p>
    Snakes are elongated, legless, carnivorous reptiles of the suborder Serpentes /sɜːrˈpɛntiːz/.[2] Like all other squamates, snakes are ectothermic, amniote vertebrates covered in overlapping scales. Many species of snakes have skulls with several more joints than their lizard ancestors, enabling them to swallow prey much larger than their heads with their highly mobile jaws. To accommodate their narrow bodies, snakes' paired organs (such as kidneys) appear one in front of the other instead of side by side, and most have only one functional lung. Some species retain a pelvic girdle with a pair of vestigial claws on either side of the cloaca. Lizards have evolved elongate bodies without limbs or with greatly reduced limbs about twenty-five times independently via convergent evolution, leading to many lineages of legless lizards.[3] Legless lizards resemble snakes, but several common groups of legless lizards have eyelids and external ears, which snakes lack, although this rule is not universal (see Amphisbaenia, Dibamidae, and Pygopodidae).
</p>

<h4>Väri esimerkkejä</h4>
<p>Tässä jotain shokkivärejä sivulle. Mutta voin sekoittaa niitä lisää -Alisa</p>
<table>
    <td style="background-color: #7dd2d9;">#7dd2d9</td>
    <td style="background-color: #33d2ce;">#33d2ce</td>
    <td style="background-color: #ff00a0;">#ff00a0</td>
    <td style="background-color: #3f3f3f;">#3f3f3f</td>
    <td style="background-color: #ffff61;">#ffff61</td>
    <td style="background-color: #ffae10;">#ffae10</td>
    <td style="background-color: #d5d5d5;">#d5d5d5</td>
</table>

<h4>Footerin värit</h4>

<table>
    <td style="background-color: #3f3f3f;">Taustaväri</td>
    <td style="background-color: #d5d5d5;">Tekstin väri</td>
    <td style="background-color: #3f3f3f; color: #d5d5d5;">Esimerkki</td>
</table>

<h4>Headerin värit</h4>
<p>Nav palkkin värit ja mitkä hooverit sinne tulisi, mutta pitää päättää eka se sivuston rakenne. Osataan katsoa väritasapainoa.</p>
<table>
    <td style="background-color: #7dd2d9;">Taustaväri</td>
    <td style="background-color: #0db371;">Linkkiväri</td>
    <td style="background-color: #3f3f3f; color: #d5d5d5;">Esimerkki</td>
    <td style="background-color: #3f3f3f; color: #d5d5d5;">Hooveri esimerkki</td>
</table>