</div>
<!-- Footer -->
<footer id="footer">
    <div class="container-fluid">
        <div class="row text-center d-flex justify-content-center">

            <div class="col-md-4">
                <i class="fas fa-phone-alt footerMainIcon"></i>
                <h6>Ota yhteyttä</h6>
                <hr class="light hr">
                <p class="footerText">Puh: +123456789</p>
                <p class="footerText">S-posti: portima@portima.fi</p>
            </div>

            <div class="col-md-4">
                <i class="fas fa-map-marker-alt footerMainIcon"></i>
                <h6>Portima Oy</h6>
                <hr class="light hr">
                <p class="footerText">Hallituskatu 17</p>
                <p class="footerText">90100 Oulu</p>
                <a href="<?= site_url('yhteystiedot') ?>" class="linkkiFooter footerText">Tietoa meistä</a>
            </div>
            <div class="col-md-4">
                <i class="fas fa-store-alt footerMainIcon"></i>
                <h6>Liikeen aukioloajat</h6>
                <hr class="light hr">
                <p class="footerText">MA-PE 10:00 - 19:00</p>
                <p class="footerText">LA 9:00 - 15:00</p>
                <p class="footerText">SU SULJETTU</p>
            </div>
            <div class="col-12 col-md-4 some_divi">
                <i class="fab fa-instagram"></i>
                <i class="fab fa-facebook-square"></i>
                <i class="fab fa-youtube"></i>
                <i class="fab fa-twitter-square"></i>

            </div>
            <div class="col-12 col-md-4"></div>
            <div class="col-12 col-md-4 maksu_divi">
                <i class="fab fa-cc-visa"></i>
                <i class="fab fa-cc-mastercard"></i>
                <i class="fab fa-cc-amex"></i>
                <i class="fab fa-cc-paypal"></i>
            </div>
            <div class="col-12 mt-4">
                <img src="<?= base_url('/../img/logo_icons/logo_valkoinen_icon.png') ?>" width="50">
            </div>
            <div class="col-12">
                <hr class='light-100 hr light'>
                <h5><a href="<?= site_url('/') ?> " class="linkkiVari">Portima.fi</a></h5>
            </div>

        </div>
    </div>
</footer>


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="<?= base_url('/js/darkMode.js') ?>"></script>
<script src="<?= base_url('/js/scroll.js') ?>"></script>
<script src="<?= base_url('/js/lightbox.js') ?>"></script>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>

</html>