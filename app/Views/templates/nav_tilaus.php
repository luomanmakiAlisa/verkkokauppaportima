<body>
    <header>
        <div class="jumbotron rounded-0">
            <div class="row col-12">
                <div class="col-6 d-flex justify-content-start">
                    <a href="<?= site_url('home/index/') ?>"><img src="<?= base_url('/../img/logo_icons/logo_valkoinen.png') ?>" height="100"></a>
                </div>
            </div>
        </div>
    </header>
    </div>
    <div class="container-fluid">
        <div class="row navPalkki d-flex justify-content-center">
            <ul class="nav">

                <li class="nav-item"><i class="fas fa-box"></i>Ilmainen toimistus</li>
                <li class="nav-item"><i class="fas fa-history"></i>Nopea toimitus</li>
                <li class="nav-item"><i class="fas fa-user-shield"></i>Turvattu verkkokauppa</li>

            </ul>

            </nav>
        </div>
    </div>
    <div class="container">