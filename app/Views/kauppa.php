<div class="row">
  <div class="col-12" id="kauppa_otsikko">
    <?php
    echo "<h3>" . $otsikko . "</h3>";
    ?>

  </div>

  <?php foreach ($tuotteet as $tuote) : ?>
    <div class="card kortti">
      <a href="<?= site_url('kauppa/tuote/' . $tuote['id']) ?>">
        <img class="kortinkuva" src="<?= base_url(['img/tuoteryhmat/' . $tuote['tuoteryhma_id'] . '/' . $tuote['kuva']]) ?>"></img>
        <h4 class="korttiOts"><?= $tuote['nimi'] ?></h4>
        <?php
        $tahti = '<i class="fas fa-star"></i>';
        $tahdet = array();

        for ($i = 0; $i < $tuote['kunto']; $i++) {
          array_push($tahdet, $tahti);
        }
        print implode($tahdet);
        ?>

        <p class="korttiHinta" id="korttiHinta"><?= $tuote['hinta'] ?> €</p>

        <form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote['id']); ?>">
          <button class="bnt btn-secondary tilaaYlos" id="lisaa"><i class="fas fa-shopping-basket"></i> Lisää ostoskoriin</button>
        </form>
      </a>
    </div>
  <?php endforeach; ?>
  <?php
  if (empty($tuotteet)) {
    echo "<div class='korkeutta col-12'>";
    echo "<h4 class='tilaaYlos'>Tästä tuoteryhmästä ei löyty tuotteita.</h4>";
    echo "<p class='tilaaYlos'>Valitse uusi tuoteryhmä.</p>";
    echo "</div>";
  }
  ?>
</div>