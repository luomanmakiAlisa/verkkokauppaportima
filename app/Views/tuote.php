<div class="container-fluid tuote">
   <div>
      <a href="<?= base_url('index.php/kauppa/index/' . $tuote['tuoteryhma_id']) ?>" class="linkkiVari2"><i class="fas fa-angle-double-left"></i>Takaisin</a>
   </div>
   <div class="row">
      <div class="col-md-6">
         <img class="tuotekuva hover-shadow" src="<?= base_url('img/tuoteryhmat/' . $tuote['tuoteryhma_id'] . '/' . $tuote['kuva']) ?>" onclick="openModal();currentSlide(1)"></img>
      </div>
      <!-- Lightbox -->
      <div id="myModal" class="modal">
         <div class="modal-content">
            <div class="mySlides">
               <div class="numbertext"><?= $tuote['nimi'] ?></div>
               <img src="<?= base_url('img/tuoteryhmat/' . $tuote['tuoteryhma_id'] . '/' . $tuote['kuva']) ?>" class="tuoteKuva">
            </div>
            <span class="close cursor" onclick="closeModal()">&times;</span>
         </div>
      </div>

      <div class="col-md-6">
         <h4><?= $tuote['nimi'] ?></h4>
         <p><?= $tuote['hinta'] ?>€</p>
         <p><?= $tuote['kuvaus']; ?></p>
         <p>Kunto:<br>
            <?php
            $tahti = '<i class="fas fa-star"></i>';
            $tahdet = array();

            for ($i = 0; $i < $tuote['kunto']; $i++) {
               array_push($tahdet, $tahti);
            }
            print implode($tahdet);
            ?> </p>
         <form method="post" action="<?= site_url('ostoskori/lisaa/' . $tuote['id']); ?>">
            <button class="bnt btn-secondary osta">Osta</button>
         </form>
      </div>
   </div>
</div>