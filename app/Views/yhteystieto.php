<div class="row mt-5 mb-5 p-1">
    <div class="col-12 col-md-5 yhteystieto">
        <h2>Tietoa meistä<span class="blink" id="blink">_</span></h2>
        <p>
            Portima on tuore käytettyjen puhelimien ja tietokoneiden myyntiin erikoistuva yritys.
            Liikkeemme sijaitsee Oulun keskustassa palveluiden keskiössä.
            Tuotteemme ovat pääasiassa yrityksiltä ostettuja laitteita, jotka ovat huollettu ja käsitelty ammattitaitoisen henkilökuntamme toimesta.
            Valikoimaamme kuuluu myös sekä käytettyjä että uusia oheislaitteita, joilla tehostat työnkulkuasi eksponentiaalisesti.
        </p>
        <p>
            Tutustu valikoimaamme verkkokaupassa, tai tule paikan päälle myymäläämme. Ammattitaitoinen ja ystävällinen henkilökuntamme neuvoo ja palvelee teitä
            tietokoneen tai muun laitteiston hankinnassa. Kierrätetty kone on ympäristöteko!
        </p>
    </div>
    <div class="col-12 col-md-2"></div>
    <div class="col-12 col-md-5 macDiv">
        <div class="divMac">
            <img src="<?= base_url("img/mainoksia/mac.png") ?>" class="mac">
        </div>
    </div>
    <div class="col-12 col-md-5 yhteystieto">
        <h2>Portima Oy</h2>
        <p>Hallituskatu 17</p>
        <p>90100 Oulu</p>
        <h5 class="mt-5">Ota yhteyttä</h5>
        <p>Puh: +123456789</p>
        <p>S-posti: portima@portima.fi</p>
        <h5 class="mt-5">Aukioloajat</h5>
        <p>MA-PE 10:00 - 19:00</p>
        <p>LA 9:00 - 15:00</p>
        <p>SU SULJETTU</p>
    </div>
    <div class="col-12 col-md-2"></div>
    <div class="col-12 col-md-5 google-maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1685.376208250456!2d25.47393971633658!3d65.01257835002902!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x468032aedcb473b9%3A0x4d7c831ce9a3b08a!2sHallituskatu%2017%2C%2090100%20Oulu!5e0!3m2!1sfi!2sfi!4v1607445558547!5m2!1sfi!2sfi" 
            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
        </iframe>
    </div>
</div>