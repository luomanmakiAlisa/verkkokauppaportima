<div class="col-12">
    <h3 class="text-center mt-5 mb-5">Lisää uusi käyttäjä</h3>
    <div class="row justify-content-lg-center">
        <form action="/adminlogin/admin_registration">
            <div class="col-12">
                <?= Config\Services::validation()->listErrors(); ?>
            </div>
            <div class="form-group">
                <input class="form-control" name="admintunnus" placeholder="Käyttäjätunnus" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="etunimi" placeholder="Etunimi" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="sukunimi" placeholder="Sukunimi" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="salasana" type="password" placeholder="Salasana" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="vahvistasalasana" type="password" placeholder="Salasana uudelleen" maxlength="30">
            </div>
            <button class="btn btn-secondary mb-5">Lisää</button>
        </form>
    </div>
</div>