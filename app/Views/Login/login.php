<body class="container-login100 rekisterointiTausta">
    <div class="col-12 logbg">

        <div class="row justify-content-sm-center mt-5 n">
            <div class="col-sm-12 col-lg-4 log pb-5">
                <div class="row justify-content-sm-center mt-4 mb-4 kirjLogo">
                    <img src="<?= base_url('/../img/logo_icons/logo_valkoinen_icon.png') ?>" width="80">
                </div>
                <h3 class="text-center mt-5 mb-4 shiro">Kirjaudu sisään</h3>
                <form action="/login/check">
                    <div>
                        <?= Config\Services::validation()->listErrors(); ?>
                    </div>
                    <div class="form-group shiro">
                        <label>Käyttäjätunnus</label>
                        <input class="form-control" name="kayttajanimi" placeholder="Syötä käyttäjätunnus" maxlength="30">
                    </div>
                    <div class="form-group shiro">
                        <label>Salasana</label>
                        <input class="form-control" name="salasana" type="password" placeholder="Syötä salasana" maxlength="30">
                    </div>
                    <div class="row">
                        <button class="btn btn-light login">Kirjaudu</button>
                        <a href="<?= site_url('login/register') ?>" class="reg">Rekisteröidy</a>
                    </div>
                </form>
                <div class="col-12 text-center">
                    <a href="<?= site_url('home/index') ?>" class="linkkiVari etusivulle">Palaa takaisin etusivulle</a>
                </div>
            </div>
        </div>
    </div>
</body>