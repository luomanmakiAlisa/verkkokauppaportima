<div class="col-12">
    <h3 class="text-center mt-5 mb-5">Rekisteröidy</h3>
    <div class="row justify-content-lg-center">
        <form action="/login/registeration">
            <div class="col-12">
                <?= Config\Services::validation()->listErrors(); ?>
            </div>
            <div class="form-group">
                <input class="form-control" name="kayttajanimi" placeholder="Käyttäjätunnus" maxlength="30">
            </div>
            <div class="form-group">
                <input class="form-control" name="etunimi" placeholder="Etunimi" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="sukunimi" placeholder="Sukunimi" maxlength="30">
            </div>
            <div class="form-group mb-5">

                <input class="form-control" name="email" type="email" placeholder="email@" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="osoite" placeholder="Osoite" maxlength="100">
            </div>
            <div class="form-row">
                <div class="form-group ml-1">
                    <input class="form-control" name="postinro" placeholder="Postinumero" maxlength="5">
                </div>
                <div class="form-group ml-4">

                    <input class="form-control" name="kaupunki" placeholder="Postitoimipaikka" maxlength="30">
                </div>
            </div>
            <div class="form-group">

                <input class="form-control" name="salasana" type="password" placeholder="Salasana" maxlength="30">
            </div>
            <div class="form-group">

                <input class="form-control" name="vahvistasalasana" type="password" placeholder="Salasana uudelleen" maxlength="30">
            </div>
            <button class="btn btn-secondary mb-5">Rekisteröidy</button>
        </form>
    </div>
</div>