<body class="container-login100 rekisterointiTausta">
    <div class="col-12 logbg">

        <div class="row justify-content-sm-center mt-5 n">
            <div class="col-sm-12 col-lg-4 log pb-5">
                <div class="row justify-content-sm-center mt-4 mb-4 kirjLogo">
                    <i class="fas fa-user-cog" style="font-size: 100px; color: white;"></i>
                </div>
                <h3 class="text-center mt-5 mb-4 shiro">Kirjaudu sisään</h3>
                <form action="/adminlogin/admin_check">
                    <div>
                        <?= Config\Services::validation()->listErrors(); ?>
                    </div>
                    <div class="form-group shiro">
                        <label>Käyttäjätunnus</label>
                        <input class="form-control" name="admintunnus" placeholder="Syötä käyttäjätunnus" maxlength="30">
                    </div>
                    <div class="form-group shiro">
                        <label>Salasana</label>
                        <input class="form-control" name="salasana" type="password" placeholder="Syötä salasana" maxlength="30">
                    </div>
                    <button class="btn btn-light login">Kirjaudu</button>
                        <a href="<?= site_url('/') ?>" class="reg">Kauppaan</a>
                    <form>
            </div>
        </div>
    </div>
</body>