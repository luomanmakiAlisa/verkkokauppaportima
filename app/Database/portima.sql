drop database if exists portima;
create database portima;
use portima;

create table asiakas (
  id int primary key auto_increment,
  kayttajanimi varchar(30) not null unique, 
  etunimi varchar(100),
  sukunimi varchar(100),
  email varchar(255) not null unique,
  osoite varchar (100) not null unique,
  postinro char(5) not null,
  kaupunki varchar (100) not null,
  salasana varchar(255) not null
);

create table yllapitaja (
  id int primary key auto_increment,
  admintunnus varchar(30) not null unique, 
  etunimi varchar(100),
  sukunimi varchar(100),
  salasana varchar(255) not null
);

create table tuoteryhma (
  id int primary key auto_increment,
  nimi varchar(50) not null unique
);

create table tuote (
  
  id int primary key auto_increment,
  nimi varchar(100) not null,
  hinta decimal(5, 2) not null,
  kuvaus text,
  kuva varchar(50) not null,
  kunto smallint not null,

  varastomaara int not null,
  tuoteryhma_id int not null,
  index tuoteryhma_id(tuoteryhma_id),
  foreign key (tuoteryhma_id) references tuoteryhma(id) on delete restrict
);


insert into tuoteryhma (id, nimi) values (1, 'Älypuhelimet');
insert into tuoteryhma (id, nimi) values (2, 'Pöytäkoneet');
insert into tuoteryhma (id, nimi) values (3, 'Kannettavat');
insert into tuoteryhma (id, nimi) values (4, 'Näytöt');
insert into tuoteryhma (id, nimi) values (5, 'Oheistuotteet');

insert into tuote (nimi,hinta,kuvaus,kuva,kunto,varastomaara,tuoteryhma_id) values 
('Harmaa iPhone 11',350,'Apple iPhone 11 mikro on maailman pienin, ohuin ja kevyin 4G-puhelin. Siinä on A200 Bionic, nopein älypuhelimen siru. Viiden kameran järjestelmä. Tyylikäs Solar Retin XDR -näyttö.', 'puhelin1.jpg', 3,6,1),
('Vaaleanpunainen iPhone 6 64GB',150,'Apple iPhone 6 vanhempaa mallia. Touch ID -sormenjälkitunnistin. Pidempi akunkesto. iPhone 6:ssa on 4,7 tuuman Retin HD -näyttö.', 'puhelin2.jpg', 2,2,1),
('Samsung Galaxy 6',150,'Samsung Galaxy 6 on tyylikäs 64-suoritimella oleva älypuhelin. Moniajo ja multimediasovellukset toimivat nopeammin ja tehokkaammin. Smart Manager -sovelluksen avulla voit tarkistaa puhelimesi tilan yhdellä vilkaisulla.', 'puhelin3.jpg', 1,3,1),
('Harmaa iPhone 12',450,'Apple iPhone 12 mikro on maailman pienin, ohuin ja kevyin 5G-puhelin. Siinä on A200 Bionic, nopein älypuhelimen siru. Seitsemän kameran järjestelmä. Tyylikäs Solar Retin XDR -näyttö.', 'puhelin4.jpg', 3,3,1),
('Samsung Galaxy S15',180,'Innovatiivisuutta huokuvaa Samsung Galaxy S15 voi ladata langattomasti. Lasipinnat on valmistettu kestävästä Gorilla 4 -lasista. nfinity-O-näyttö päästää luovuutesi valloilleen – voit tehdä taustakuvia, joissa huomioidaan kameran aukko; tai voit valita valmiita taustakuvia laajasta valikoimastamme.', 'puhelin5.jpg', 3,6,1),
('Huawei P10 Pro 4G',100,'Huawei P10 Pro 4G on vanhempaa mallia, mutta toimii hyvin kiitettävästi sille joka ei kaipaa uudempien älypuhelimien monimutkaisuutta.', 'puhelin6.jpg', 2,10,1),
('Samsung P80 Pro 5G',500.01,'Samsung P80 Pro 5G on valmiina tulevaisuuteen, sillä siinä on valmius uuden sukupolven 5G-yhteyksille. Kuririn 9999 G5 -prosessoria täydentävät 8 Gt:n keskusmuisti sekä 256 Gt:n muisti. ', 'puhelin7.jpg', 3,1,1),
('Huawei P24 lite',80,'Huawei P24 Lite tarjoaa runsaasti tallennustilaa. 128 Gt:n muistiin mahtuvat lempisovelluksesi, valokuvat ja videot, ja kasvata tallennustilaa ulkoisella muistikortilla aina 512 Gt:n asti. 4 Gt:n keskusmuisti takaa, että vaihdat sovelluksesta toiseen sulavasti.', 'puhelin8.jpg', 2,3,1),
('Huawei Ps34',200,'Huawei Ps29 Lite ei jää isompiensa varjoon. Huawei Ps29 Lite tarjoaa lippulaivapuhelimista tutut ominaisuudet: vaikuttava 48+8+2 MP:n kymppikamera, jossa 150°:n ultralaajakulma ja kehittynyt zoomaus, 34 MP:n selfiekamera sekä upea 6,15” teräväpiirtonäyttö.', 'puhelin9.jpg', 1,1,1),
('Catepillar H34 5GT',270,'Joka säänkestävä älypuhelin. Iskunkestävä näyttö. Erinomainen kamera, jolla saat kauniit kuvat. Metsänkulkijoiden suosikki. ', 'puhelin10.jpg', 3,2,1);

insert into tuote (nimi,hinta,kuvaus,kuva,kunto,varastomaara,tuoteryhma_id) values 
('Macbook Air 13” M1 256 Gt',500,'Applen ohuin ja kevein kannettava. Moottorina supervahva M1-siru.','L1,muok.jpg',3,5,3),
('Macbook Air 13” M1 512 Gt',700,'Applen ohuin ja kevein kannettava. Moottorina supervahva M1-siru.','bokmaac.jpg',4,5,3),
('MacBook Pro 13” M1 512 Gt ',900,'Applen M1-siru määrittelee 13 tuuman MacBook Pron uudelleen. 8-ytiminen prosessori suoriutuu monimutkaisista valokuvauksen, koodauksen ja videoeditoinnin työnkuluista leikiten.','L2muok.jpg',4,4,3),
('Lenovo ThinkPad A285 12,5"',400,'Äärimmäisen kompakti ja kestävä. Kokonaan uudelleen suunniteltu Lenovo ThinkPad A285 on paljon matkaavan ammattilaisen unelmakannettava. ','L10muok.jpg',3,5,3),
('Asus M509 15,6"',250,'ASUS M509 on tyylikäs ja tehokas 15,6" multimediakannettava, joka on varustettu erinomaisella suorituskyvyllä ja huippuohuella, useimmiten vain kalliimmista kannettavista tietokoneista tutulla muotoilulla.','L3muok.jpg',2,5,3),
('Dell r45',350,'Dell Latitude 3301 on elegantti ja kevyt kannettava työhön ja vapaa-aikaan. Tyylikäs ohutraaminen Full HD -näyttö tarjoaa kirkkaan ja terävän kuvan.','dell.jpg',2,5,3),
('HP EliteBook 840 G6 14"',800,'HP EliteBook 840 on suunniteltu tämän päivän liikkuville ammattilaisille. Erittäin turvalliset ja helposti hallittavat yhteistyötoiminnot auttavat yrityksen henkilöstöä parantamaan tuottavuutta ja tietoturvaa niin toimistossa kuin sen ulkopuolella.','L8muok.jpg',4,5,3),
('HP 340s G7 14" -kannettava',500,'Kauniisti muotoiltu ja tehokkailla ominaisuuksilla varustettu, edullisesti hinnoiteltu HP 340S -kannettava on ohut, kevyt ja valmis auttamaan liiketoiminnan vaatimusten täyttämisessä.','L5muok.jpg',3,5,3);

insert into tuote (nimi,hinta,kuvaus,kuva,kunto,varastomaara,tuoteryhma_id) values 
('Acer SA270Abi 27"', 140, "Full HD näyttö toimistoon tai vapaa-ajan käyttöön. 75Hz, 4ms vasteaika. Mukana seinäteline.", "acer.jpg", 4, 6, 4),
('Alienware 34"', 220, "Alienwaren suuri ja kaareva pelinäyttö takaa täyden immersion ihmeelliseen pelimaailmaan. 21:9 kuvasuhde, 2ms vasteaika ja 1900R WQHD resoluutio.", "alienware.jpg", 4, 3, 4),
('Dell Office4000 24"', 100, "Luotettava työkalu peruskäyttäjälle toimistotyöhön. Energialuokka A++.", "dell.jpg", 4, 8, 4),
('Dell Office3200 24"', 80, "Ikoninen Dellin malli, joka julkaisuvuotena 2008 niitti menestystä monissa näyttövertailuissa. Office4000:n edeltäjä.", "dell2.jpg", 4, 12, 4),
('Dell Universum9000 32"', 200, "Dellin uusin lippulaivamalli ammattilaisgraafikon tai vaativan harrastajan käyttöön. Mustaakin mustempi musta ja värien toisto omaa luokkaansa. 4k resoluutio. Vain rajoitettu erä.", "dell3.jpg", 5, 2, 4),
('HP Museum 22"', 49, "Monitori ilman turhia erikoisuuksia. Kestävä muovirunko.", "hp.jpg", 3, 8, 4),
('Lenovo QWERTY24 24"', 120, "Laatunäyttö FullHD resoluutiolla. 1ms vasteaika.", "lenovo.jpg", 5, 5, 4),
('LG AX2411 24"', 100, "Näyttö se on tämäkin, eikä huono ollenkaan. Osta pois.", "lg.jpg", 4, 1, 4),
('Simsung Simsalabim 27"', 160, "Viihdekäyttäjän näyttö viihdekäyttöön. Kunto kuin uusi.", "simsung.jpg", 5, 4, 4),
('Susa N800 24"', 90, "Peruskäyttäjän perusnäyttö. Perinteisen tyylikäs muotoilu ja teräksinen näytönjalka pitää ruudun suorassa.", "susa.jpg", 3, 3, 4);

insert into tuote (nimi,hinta,kuvaus,kuva,kunto,varastomaara,tuoteryhma_id) values 
("Logitech Office-hiiri", 30, "Luotettava ja käteenistuva perushiiri toimistokäyttöön. Tuote on uusi.", "hiiri1.jpg", 5, 20, 5),
("Apple-hiiri", 80, "Erä uusia Applen hiiriä saapui kauppaamme. Taattua Apple-laatua Macin omistajille.", "hiiri2.jpg", 5, 15, 5),
("Apple-näppäimistö", 150, "Erä uusia Applen näppäimistöjä saapui kauppaamme. Taattua Apple-laatua Macin omistajille.", "nappaimisto1.jpg", 5, 15, 5),
("Logitech Office-näppäimistö", 60, "Logitechin näppäimistö ankaraankin toimistotyöhön. Tässä kohtaavat laatu ja hinta. Tuote on uusi.", "nappaimisto2.jpg", 5, 20, 5),
("DataTraveler USB-tikku 8GB", 15, "DataTravelerin uusin luomus, jossa metsäinen teema. Muistia kapistuksessa 8GB. Tuote on uusi.", "usb.jpg", 5, 20, 5),
("SanDisk ulkoinen SSD 500Gt", 110, "Lisätilaa SanDiskin 500Gt:n ulkoisesta SSD-levystä. Tuote on uusi", "ssd.jpg", 5, 18, 5),
("JBL Bluetooth-kaiutin", 60, "Tästä matkalle JBL:n langaton Boombox. Tuote on uusi", "kaiutin1.jpg", 5, 10, 5),
("Philips Bluetooth-kaiutin", 50, "Philipsin langaton kaiutin vaikka yöpöydälle. Tuote on uusi", "kaiutin2.jpg", 5, 15, 5),
("Beats Powerbeats langattomat kuulokkeet", 70, "Rokkaa tiesi huipulle Powerbeatsin langattomilla kuulokkeilla. Tuote on uusi", "kuuloke1.jpg", 5, 20, 5),
("Beats Powerbeats langalliset kuulokkeet", 50, "Powerbeatsin langalliset kuulokkeet ja värinä tietenkin pinkki. Tuote on uusi", "kuuloke2.jpg", 5, 25, 5);

create table tilaus (
  id int primary key auto_increment,
  paivays timestamp default current_timestamp,
  asiakas_id int not null,
  index asiakas_id(asiakas_id),
  foreign key (asiakas_id) references asiakas(id)
  on delete restrict
);

create table tilausrivi (
  tilaus_id int not null,
  index tilaus_id(tilaus_id),
  foreign key (tilaus_id) references tilaus(id)
  on delete restrict,
  tuote_id int not null,
  index tuote_id(tuote_id),
  foreign key (tuote_id) references tuote(id)
  on delete restrict,
  maara smallint unsigned not null
);


insert into asiakas (kayttajanimi,etunimi,sukunimi,email,osoite,postinro,kaupunki,salasana) values
("liisakoko", "Liisa" , "Kokkola", "liisa.kokkola@tasu.fi", "Lissukantie 3", "12334", "Kokkola", "$2y$10$UW8S3IOFz/XzYfOrGQzniea8LZX2bPNAnfbRW6rhrQMIdwXCklB/m");
/* liisakoko; 12345678*/

insert into yllapitaja (admintunnus, etunimi, sukunimi, salasana) values
("admin1234", "Aatu", "Admini", "$2y$10$KiEuyHTTMMHWCQSrJkBWBuDslTldF7Zd93ziQhgOToDjA2jQl1/TO");
/*Käyttäjä: admin1234 ja salasana: admin1234*/


/*Esimerkkitilaus*/
insert into tilaus (asiakas_id) values 
("1");

insert into tilausrivi (tilaus_id, tuote_id, maara) values
("1", "2", "3");