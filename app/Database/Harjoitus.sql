drop database if exists keilahalli;
create database keilahalli;
use keilahalli;

create table VARAUSTAPA (
    id int primary key auto_increment,
    tapa varchar(30)
);

create table NIMIKE (
  id int primary key auto_increment,
  nimike varchar(64) not null
);

create table AIKA_ALENNUS (
  id int primary key auto_increment,
  aloitusaika time not null,
  lopetusaika time not null,
  alennus decimal(5,2) not null
);

create table ALENNUSRYHMA (
  id int primary key auto_increment,
  nimi varchar(64) not null, 
  aloituspvm date not null,
  alennus_hinnasta decimal(5,2) not null
);

create table HUOLTO (
  id int primary key auto_increment,
  nimi varchar(20) not null
);

create table POSTI (
  id int primary key auto_increment,
  postinro char(5) not null,
  postitmp varchar (100) not null UNIQUE
);

create table ASIAKAS (
  id int primary key auto_increment,
  yritys varchar(100) null, 
  etunimi varchar(64) not null,
  sukunimi varchar(64) not null,
  email varchar(255) not null unique,
  osoite varchar (100) null,
  posti_id int not null,
  puhelinro varchar(20),
  alennusryhma_id int,
  foreign key (posti_id) references posti(id) on delete restrict,
  foreign key (alennusryhma_id) references alennusryhma(id) on delete restrict
);


create table TOIMIPAIKKA (
    id int primary key auto_increment,
    nimi varchar(64) not null,
    osoite varchar(120) not null,
    puhelinro varchar(20) not null,
    posti_id int not null,
    foreign key (posti_id) references posti(id) on delete restrict
);


create table TYONTEKIJA (
  id int primary key auto_increment,
  etunimi varchar(64) not null,
  sukunimi varchar(64) not null,
  puhelinro varchar(20) not null,
  posti_id int not null,
  email varchar(255) not null,
  osoite varchar(255) not null,
  toimipaikka_id int not null,
  foreign key (posti_id) references posti(id) on delete restrict,
  foreign key (toimipaikka_id) references toimipaikka(id) on delete restrict
);

create table TYOSUHDE (
  id int primary key auto_increment,
  apvm date not null,
  lpvm date not null,
  esimies_id int,
  tyontekija_id int not null,
  foreign key (tyontekija_id) references tyontekija(id) on delete restrict
);

create table TYONNIMIKE (
  id int primary key auto_increment,
  tyosuhde_id int not null,
  nimike_id int not null,
  foreign key (tyosuhde_id) references tyosuhde(id) on delete restrict,
  foreign key (nimike_id) references nimike(id) on delete restrict
);

create table HINTA (
  id int primary key auto_increment,
  hinta decimal(5,2) not null, 
  aloituspvm date not null,
  alennusryhma_id int,
  alv decimal(5,2) not null,
  aika_alennus_id int not null,
  foreign key (aika_alennus_id) references aika_alennus(id) on delete restrict,
  foreign key (alennusryhma_id) references alennusryhma(id) on delete restrict
);

create table KATKO (
  id int primary key auto_increment,
  keilarata_id int not null, 
  apvm date not null,
  lpvm date not null,
  aklo time not null,
  lklo time not null,
  huolto_id int not null,
  foreign key (huolto_id) references huolto(id) on delete restrict
);

create table KEILARATA (
    id int primary key auto_increment,
    toimipaikka_id int not null,
    hinta_id int not null,
    foreign key (toimipaikka_id) references toimipaikka(id) on delete restrict,
    foreign key (hinta_id) references hinta(id) on delete restrict
);

create table VARAUS (
  id int primary key auto_increment,
  asiakas_id int not null,
  varauspvm timestamp,
  varaustapa_id int not null,
  foreign key (asiakas_id) references asiakas(id) on delete restrict,
  foreign key (varaustapa_id) references varaustapa(id) on delete restrict
);

create table VARAUSRIVI (
    id int primary key auto_increment,
    varaus_id int not null,
    keilarata_id int not null,
    varattupvm date not null,
    aloitusaika time not null,
    lopetusaika time not null,
    maksettu boolean,
    foreign key (varaus_id) references varaus(id) on delete restrict,
    foreign key (keilarata_id) references keilarata(id) on delete restrict
);